%global debug_package   %{nil}
%global __spec_install_post %{nil}

Name:          qt5-doc
Version:       5.15.1
Release:       1
Summary:       QT5 application interface (API) documentation
BuildArch:     noarch
License:       GFDL-1.3-only
Url:           http://qt-project.org/
Source0:       qt-doc-opensource-src-%{version}.tar.xz
BuildRequires: qt5-rpm-macros >= 5.5.0
Obsoletes: qt5-qtcanvas3d-doc < 5.13

Requires: qt5-qtbase-doc >= %{version} qt5-qt3d-doc >= %{version}
Requires: qt5-qtcharts-doc >= %{version} qt5-qtconnectivity-doc >= %{version} qt5-qtdeclarative-doc >= %{version}
Requires: qt5-qtgraphicaleffects-doc >= %{version} qt5-qtimageformats-doc >= %{version} qt5-qtlocation-doc >= %{version}
Requires: qt5-qtmultimedia-doc >= %{version} qt5-qtquickcontrols2-doc >= %{version} qt5-qtquickcontrols-doc >= %{version}
Requires: qt5-qtscript-doc >= %{version} qt5-qtscxml-doc >= %{version} qt5-qtsensors-doc >= %{version}
Requires: qt5-qtserialbus-doc >= %{version} qt5-qtserialport-doc >= %{version} qt5-qtsvg-doc >= %{version}
Requires: qt5-qttools-doc >= %{version} qt5-qtvirtualkeyboard-doc >= %{version} qt5-qtwebchannel-doc >= %{version}
Requires: qt5-qtwebsockets-doc >= %{version} qt5-qtx11extras-doc >= %{version}
Requires: qt5-qtxmlpatterns-doc >= %{version} qt5-qtdatavis3d-doc >= %{version} qt5-qtgamepad-doc >= %{version}
Requires: qt5-qtlocation-doc >= %{version} qt5-qtwayland-doc >= %{version} qt5-qtwebview-doc >= %{version}
Requires: qt5-qtspeech-doc >= %{version} qt5-qtremoteobjects-doc >= %{version} qt5-qtcharts-doc >= %{version}
Requires: qt5-qtpurchasing-doc >= %{version}

%description
This package contains all qt5 application interface documents.

%package -n qt5-qtbase-doc
Summary: Documentation for qtbase

%description -n qt5-qtbase-doc
%{summary}.

%package -n qt5-qt3d-doc
Summary: Documentation for qt3d

%description -n qt5-qt3d-doc
%{summary}.

%package -n qt5-qtcharts-doc
Summary: Documentation for qtcharts

%description -n qt5-qtcharts-doc
%{summary}.

%package -n qt5-qtconnectivity-doc
Summary: Documentation for qtconnectivity

%description -n qt5-qtconnectivity-doc
%{summary}.

%package -n qt5-qtdeclarative-doc
Summary: Documentation for qtdeclarative

%description -n qt5-qtdeclarative-doc
%{summary}.

%package -n qt5-qtgraphicaleffects-doc
Summary: Documentation for qtgraphicaleffects

%description -n qt5-qtgraphicaleffects-doc
%{summary}.

%package -n qt5-qtimageformats-doc
Summary: Documentation for qtimageformats

%description -n qt5-qtimageformats-doc
%{summary}.

%package -n qt5-qtmultimedia-doc
Summary: Documentation for qtmultimedia

%description -n qt5-qtmultimedia-doc
%{summary}.

%package -n qt5-qtquickcontrols2-doc
Summary: Documentation for qtquickcontrols2

%description -n qt5-qtquickcontrols2-doc
%{summary}.

%package -n qt5-qtquickcontrols-doc
Summary: Documentation for qtquickcontrols

%description -n qt5-qtquickcontrols-doc
%{summary}.

%package -n qt5-qtscript-doc
Summary: Documentation for qtscript

%description -n qt5-qtscript-doc
%{summary}.

%package -n qt5-qtscxml-doc
Summary: Documentation for qtscxml

%description -n qt5-qtscxml-doc
%{summary}.

%package -n qt5-qtsensors-doc
Summary: Documentation for qtsensors

%description -n qt5-qtsensors-doc
%{summary}.

%package -n qt5-qtserialbus-doc
Summary: Documentation for qtserialbus

%description -n qt5-qtserialbus-doc
%{summary}.

%package -n qt5-qtserialport-doc
Summary: Documentation for qtserialport

%description -n qt5-qtserialport-doc
%{summary}.

%package -n qt5-qtsvg-doc
Summary: Documentation for qtsvg

%description -n qt5-qtsvg-doc
%{summary}.

%package -n qt5-qttools-doc
Summary: Documentation for qttools

%description -n qt5-qttools-doc
%{summary}.

%package -n qt5-qtvirtualkeyboard-doc
Summary: Documentation for qtvirtualkeyboard

%description -n qt5-qtvirtualkeyboard-doc
%{summary}.

%package -n qt5-qtwebchannel-doc
Summary: Documentation for qtwebchannel

%description -n qt5-qtwebchannel-doc
%{summary}.

%package -n qt5-qtwebsockets-doc
Summary: Documentation for qtwebsockets

%description -n qt5-qtwebsockets-doc
%{summary}.

%package -n qt5-qtx11extras-doc
Summary: Documentation for qtx11extras

%description -n qt5-qtx11extras-doc
%{summary}.

%package -n qt5-qtspeech-doc
Summary: Documentation for qtspeech

%description -n qt5-qtspeech-doc
%{summary}.

%package -n qt5-qtremoteobjects-doc
Summary: Documentation for qtremoteobjects

%description -n qt5-qtremoteobjects-doc
%{summary}.

%package -n qt5-qtpurchasing-doc
Summary: Documentation for qtpurchasing

%description -n qt5-qtpurchasing-doc
%{summary}.

%package -n qt5-qtwayland-doc
Summary: Documentation for qtwayland

%description -n qt5-qtwayland-doc
%{summary}.

%package -n qt5-qtwebview-doc
Summary: Documentation for qtwebview

%description -n qt5-qtwebview-doc
%{summary}.

%package -n qt5-qtlocation-doc
Summary: Documentation for qtlocation

%description -n qt5-qtlocation-doc
%{summary}.

%package -n qt5-qtxmlpatterns-doc
Summary: Documentation for qtxmlpatterns

%description -n qt5-qtxmlpatterns-doc
%{summary}.

%package -n qt5-qtdatavis3d-doc
Summary: Documentation for qtdatavis3d

%description -n qt5-qtdatavis3d-doc
%{summary}.

%package -n qt5-qtgamepad-doc
Summary: Documentation for qtgamepad

%description -n qt5-qtgamepad-doc
%{summary}.

%install
install -d %{buildroot}
tar xf %{SOURCE0} -C %{buildroot}/
## unpackaged files
pushd %{buildroot}%{_qt5_docdir}
rm -rfv \
  qdoc* \
  qtdistancefieldgenerator* \
  qtdoc* qtcmake* \
  qtlottieanimation* \
  qtpdf* \
  qtwebengine*

popd

%files -n qt5-qtbase-doc
%{_qt5_docdir}/{qmake*,qtconcurrent*,qtcore*,qtdbus*,qtgui*,qtnetwork*,qtopengl*,qtplatformheaders*,qtprintsupport*,qtsql*,qtwidgets*,qtxml*,qttestlib*}

%files -n qt5-qt3d-doc
%{_qt5_docdir}/qt3d*

%files -n qt5-qtcharts-doc
%{_qt5_docdir}/{qtcharts*,qtlabs*}

%files -n qt5-qtconnectivity-doc
%{_qt5_docdir}/{qtbluetooth*,qtnfc*}

%files -n qt5-qtdeclarative-doc
%{_qt5_docdir}/{qtqml*,qtquick*}

%files -n qt5-qtgraphicaleffects-doc
%{_qt5_docdir}/qtgraphicaleffects*

%files -n qt5-qtimageformats-doc
%{_qt5_docdir}/qtimageformats*

%files -n qt5-qtmultimedia-doc
%{_qt5_docdir}/qtmultimedia*

%files -n qt5-qtquickcontrols2-doc
%{_qt5_docdir}/qtquickcontrols/

%files -n qt5-qtquickcontrols-doc
%{_qt5_docdir}/qtquickcontrols1/

%files -n qt5-qtscript-doc
%{_qt5_docdir}/qtscript*

%files -n qt5-qtscxml-doc
%{_qt5_docdir}/qtscxml*

%files -n qt5-qtsensors-doc
%{_qt5_docdir}/qtsensors*

%files -n qt5-qtserialbus-doc
%{_qt5_docdir}/qtserialbus*

%files -n qt5-qtserialport-doc
%{_qt5_docdir}/qtserialport*

%files -n qt5-qtsvg-doc
%{_qt5_docdir}/qtsvg*

%files -n qt5-qttools-doc
%{_qt5_docdir}/{qdoc*,qtassistant*,qtdesigner*,qthelp*,qtlinguist*,qtuitools*}

%files -n qt5-qtvirtualkeyboard-doc
%{_qt5_docdir}/qtvirtualkeyboard*

%files -n qt5-qtwebchannel-doc
%{_qt5_docdir}/qtwebchannel*

%files -n qt5-qtwebsockets-doc
%{_qt5_docdir}/qtwebsockets*

%files -n qt5-qtx11extras-doc
%{_qt5_docdir}/qtx11extras*

%files -n qt5-qtspeech-doc
%{_qt5_docdir}/qtspeech*

%files -n qt5-qtremoteobjects-doc
%{_qt5_docdir}/qtremoteobjects*

%files -n qt5-qtpurchasing-doc
%{_qt5_docdir}/qtpurchasing*

%files -n qt5-qtwayland-doc
%{_qt5_docdir}/qtwayland*

%files -n qt5-qtwebview-doc
%{_qt5_docdir}/qtwebview*

%files -n qt5-qtlocation-doc
%{_qt5_docdir}/{qtlocation*,qtpositioning*}

%files -n qt5-qtxmlpatterns-doc
%{_qt5_docdir}/qtxmlpatterns*

%files -n qt5-qtdatavis3d-doc
%{_qt5_docdir}/{qtdatavis3d*,qtdatavisualization*}

%files -n qt5-qtgamepad-doc
%{_qt5_docdir}/qtgamepad*

%changelog
* Tue Aug 8 2023 zhangchenglin<zhangchenglin@kylinos.cn> - 5.15.1-1
- Update to version 5.15.1

* Mon Jul 18 2022 yaoxin <yaoxin30@h-partners.com> - 5.11.1-5
- License comliance rectification

* Fri Nov 29 2019 shijian <shijian16@huawei.com> - 5.11.1-4
- Package init
